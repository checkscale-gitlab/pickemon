"""
This module provides the test for the server.
"""
import argparse
import logging
import sys
import unittest

import grpc.experimental

logging.basicConfig(format='%(asctime)s %(levelname)s %(message)s',
                    datefmt='%m/%d/%Y %H:%M:%S',
                    level=logging.DEBUG)


class TestServer(unittest.TestCase):
    """
    Test cases for the pickemon server.
    """

    POKEMON_MEWTWO= {
        'name': 'mewtwo',
        'description': 'It was created by a scientist after years of horrific gene splicing '
                       'and DNA engineering experiments.',
        'habitat': 'rare',
        'is_legendary': True
    }

    def __init__(self, test_name: str, host: str, port: int) -> None:
        super(TestServer, self).__init__(test_name)
        self.host = host
        self.port = port

    def setUP(self) -> None:
        pass

    def tearDown(self) -> None:
        pass

    def test_grpc_get_pokemon(self) -> None:
        """
        Makes a gRPC call to get information about an existing Pokemon.
        """
        protos = grpc.protos("proto/pokemon.proto")
        services = grpc.services("proto/pokemon.proto")

        response = services.PokemonRetriever.get(protos.PokemonRequest(name=self.POKEMON_MEWTWO['name']),
                                                 f'{self.host}:{self.port}',
                                                 insecure=True)
        self.assertEqual(response.name, self.POKEMON_MEWTWO['name'])
        self.assertEqual(response.description, self.POKEMON_MEWTWO['description'])
        self.assertEqual(response.habitat, self.POKEMON_MEWTWO['habitat'])
        self.assertEqual(response.is_legendary, self.POKEMON_MEWTWO['is_legendary'])


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--host', dest='host', help='The host', required=True)
    parser.add_argument('--port', dest='port', help='The port', required=True)
    parsed_arguments = parser.parse_args()
    logging.info(f"Running tests, looking for pickemon at {parsed_arguments.host}:{parsed_arguments.port}")

    # Loads the test names
    test_loader = unittest.TestLoader()
    test_names = test_loader.getTestCaseNames(TestServer)

    # Adds the tests to the test suite
    suite = unittest.TestSuite()
    for test_name in test_names:
        suite.addTest(TestServer(test_name, host=parsed_arguments.host, port=parsed_arguments.port))

    # Runs the test suite
    result = unittest.TextTestRunner().run(suite)
    sys.exit(not result.wasSuccessful())
