# syntax=docker/dockerfile:1

# Base image
FROM python:latest
EXPOSE 6000

# Use app as a default location for all the subsequent commands
WORKDIR /pickemon

# Copy source code
COPY src src

# Install requirements and prepare the environment
COPY requirements.txt requirements.txt
COPY setup.py setup.py
RUN pip install -r requirements.txt
RUN pip install -e .

# Run the web server
WORKDIR /pickemon/src
CMD ["python3", "server.py", "--host", "0.0.0.0", "--port", "6000"]
